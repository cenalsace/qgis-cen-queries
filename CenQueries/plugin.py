#!/usr/bin/python3

import re
import secrets

import CenQueries as cq
from db_manager.db_plugins.plugin import BaseError
from db_manager.db_plugins.postgis.connector import PostGisDBConnector
from PyQt5.QtCore import QAbstractTableModel, QObject, QSortFilterProxyModel, Qt
from PyQt5.QtWidgets import QHBoxLayout, QLineEdit, QTableView, QVBoxLayout, QWidget
from qgis.core import Qgis, QgsDataSourceUri, QgsProject, QgsSettings, QgsVectorLayer

CONNECTION_NAME = "DATA"


def getConnectorFromUri(connection_name: str) -> "DBConnector":
    """
    Set connector property
    for the given database type
    and parameters
    """
    connector = None
    params = QgsSettings()
    params.beginGroup(f"/PostgreSQL/connections/{connection_name}")
    cfg = {}
    cfg["aDatabase"] = params.value("database")
    cfg["aHost"] = params.value("host")
    cfg["aPort"] = params.value("port")
    cfg["authConfigId"] = params.value("authcfg")
    cfg["aUsername"] = None
    cfg["aPassword"] = None

    uri = QgsDataSourceUri()
    uri.setConnection(**cfg)

    if Qgis.QGIS_VERSION_INT >= 31200:
        # we need a fake DBPlugin object
        # with connectionName and providerName methods
        obj = QObject()
        obj.connectionName = lambda: "fake"
        obj.providerName = lambda: "postgres"

        connector = PostGisDBConnector(uri, obj)
    else:
        connector = PostGisDBConnector(uri)

    return connector


def fetchDataFromSqlQuery(connector: "DBConnector", sql: str) -> list:
    """
    Execute an SQL query and
    return [data, rowCount, ok]
    NB: commit qgis/QGIS@14ab5eb changes QGIS DBmanager behaviour
    """
    data = []
    row_count = 0
    c = None
    ok = True
    try:
        c = connector._execute(None, str(sql))
        data = connector._fetchall(c)
        row_count = len(data)

    except BaseError as e:
        ok = False

    finally:
        if c:
            c.close()
            del c

    return [data, row_count, ok]


class TableModel(QAbstractTableModel):
    def __init__(self, query: str, headers: list, parent=None):
        """Create a new instance
        :param query: function that returns model data as a Pandas DataFrame
        :param params: dict with model query arguments
        :type parent: QObject
        :param parent: model parent, used to display waiting messages when loading
        """
        # inherits all members from QAbstractTableModel
        super(TableModel, self).__init__(parent)
        # attributes
        self.connector = getConnectorFromUri("data")
        self.query = query
        self.headers = headers
        self.records, self.row_count, ok = fetchDataFromSqlQuery(
            connector=self.connector, sql=self.query
        )
        if not ok:
            raise ValueError("Error in the query")

    def rowCount(self, parent):
        return self.row_count

    def columnCount(self, parent):
        return len(self.headers)

    def data(self, index, role):
        if not index.isValid() or index.row() < 0 or index.row() > self.row_count:
            return None

        if role == Qt.DisplayRole:
            return self.records[index.row()][index.column()]

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return self.headers[section]

            if role == Qt.TextAlignmentRole:
                return Qt.AlignLeft | Qt.AlignVCenter

        if orientation == Qt.Vertical and role == Qt.DisplayRole:
            return section + 1

        return None


class MultiFilterProxyModel(QSortFilterProxyModel):
    """source: https://stackoverflow.com/a/57845903"""

    AND, OR = range(2)

    def __init__(self, *args, **kwargs):
        QSortFilterProxyModel.__init__(self, *args, **kwargs)
        self.filters = {}
        self.multi_filter_mode = self.OR

    def setFilterByColumn(self, column, regex):
        if isinstance(regex, str):
            regex = re.compile(regex)

        self.filters[column] = regex
        self.invalidateFilter()

    def clearFilter(self, column):
        del self.filters[column]
        self.invalidateFilter()

    def clearFilters(self):
        self.filters = {}
        self.invalidateFilter()

    def filterAcceptsRow(self, source_row, source_parent):
        if not self.filters:
            return True

        results = []
        for key, regex in self.filters.items():
            text = ""
            index = self.sourceModel().index(source_row, key, source_parent)
            if index.isValid():
                text = self.sourceModel().data(index, Qt.DisplayRole)
                if text is None:
                    text = ""
            results.append(regex.search(text.lower()))

        if self.multi_filter_mode == self.OR:
            return any(results)

        return all(results)


class CenQueriesWindow(QWidget):

    (
        ID,
        LABEL,
        TAGS,
        CREATED,
        FIRSTNAME,
        NAME,
        UNIQUE,
        GEOM,
        QUERY,
    ) = range(9)

    def __init__(self, iface, parent=None) -> None:
        super(CenQueriesWindow, self).__init__(parent)
        self.setWindowTitle("Extractions CEN")
        self.iface = iface
        self.data_filter = QLineEdit()
        self.data_filter.setPlaceholderText("Rechercher ...")
        self.model_qry = (
            "SELECT e.id, e.libelle, array_to_string(e.tags, ', '), e.date_creation, u.prenom, u.nom,"
            " e.unique_field, e.geom_field, e.requete"
            " FROM cen.extractions e INNER JOIN cen.utilisateurs u ON e.user_id = u.id"
        )
        self.model_headers = {
            self.ID: {"header": "id", "width": 0, "hidden": True},
            self.LABEL: {"header": "Libellé", "width": 300, "hidden": False},
            self.TAGS: {"header": "tags", "width": 150, "hidden": False},
            self.CREATED: {"header": "Date de création", "width": 100, "hidden": False},
            self.FIRSTNAME: {"header": "Prénom", "width": 150, "hidden": False},
            self.NAME: {"header": "Nom", "width": 150, "hidden": False},
            self.UNIQUE: {"header": "unique_field", "width": 0, "hidden": True},
            self.GEOM: {"header": "geom", "width": 0, "hidden": True},
            self.QUERY: {"header": "requete", "width": 0, "hidden": True},
        }
        model = TableModel(
            query=self.model_qry,
            headers=[p["header"] for p in self.model_headers.values()],
        )
        self.tv = QTableView()
        self.tv.setSortingEnabled(True)
        self.tv.horizontalHeader().setStretchLastSection(True)
        self.tv.setAlternatingRowColors(True)
        proxy_model = MultiFilterProxyModel()
        proxy_model.setSourceModel(model)
        self.tv.setModel(proxy_model)
        self.tv.sortByColumn(0, Qt.AscendingOrder)
        self.tv.setWordWrap(False)
        for field, params in self.model_headers.items():
            self.tv.setColumnWidth(field, params["width"])
            self.tv.setColumnHidden(field, params["hidden"])

        # layout
        v_lyt = QVBoxLayout()
        self.filter_lyt = QHBoxLayout()
        v_lyt.addWidget(self.data_filter)
        v_lyt.addLayout(self.filter_lyt)
        v_lyt.addWidget(self.tv)
        self.setLayout(v_lyt)
        self.resize(1000, 300)
        # signals
        self.data_filter.editingFinished.connect(self.filter_view)
        self.tv.doubleClicked.connect(self.execute_query)

    def filter_view(self):
        filter = self.data_filter.text().lower().replace(" ", "|")
        for field, params in self.model_headers.items():
            if not params["hidden"]:
                self.tv.model().setFilterByColumn(field, filter)

    def execute_query(self, index):
        clicked_row = index.row()
        key_idx = self.tv.model().index(clicked_row, self.UNIQUE)
        label_idx = self.tv.model().index(clicked_row, self.LABEL)
        geom_idx = self.tv.model().index(clicked_row, self.GEOM)
        query_idx = self.tv.model().index(clicked_row, self.QUERY)
        key = self.tv.model().data(key_idx)
        label = self.tv.model().data(label_idx)
        geom = self.tv.model().data(geom_idx)
        query = self.tv.model().data(query_idx)
        if query[-1] == ";":
            query = query[:-1]

        params = QgsSettings()
        params.beginGroup(f"/PostgreSQL/connections/{CONNECTION_NAME}")
        cfg = {}
        cfg["aDatabase"] = params.value("database")
        cfg["aHost"] = params.value("host")
        cfg["aPort"] = params.value("port")
        cfg["authConfigId"] = params.value("authcfg")
        cfg["aUsername"] = None
        cfg["aPassword"] = None

        uri = QgsDataSourceUri()
        uri.setConnection(**cfg)
        uri.setDataSource("", f"({query})", geom, "")
        uri.setKeyColumn(key)

        token = secrets.token_urlsafe(3)
        extract_name = f"{cq.title_normalize(label)}_{token}"
        vlayer = QgsVectorLayer(uri.uri(False), extract_name, "postgres")
        if vlayer.isValid():
            QgsProject.instance().addMapLayer(vlayer)
            self.iface.messageBar().pushMessage(
                f"Extraction {label} bien chargée : {extract_name}",
                Qgis.Success,
                duration=10,
            )
        else:
            self.iface.messageBar().pushMessage(
                f"Extraction {label} n'a pas été chargée : {extract_name}",
                Qgis.Warning,
                duration=10,
            )
