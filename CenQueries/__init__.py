#!/usr/bin/python3
# -----------------------------------------------------------
# Copyright (C) 2022 CEN Alsace
# -----------------------------------------------------------
# Licensed under the terms of GNU AGPL
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os
import unicodedata as uni

from CenQueries.plugin import CenQueriesWindow
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QToolBar


def classFactory(iface):
    return CenQueriesPlugin(iface)


def title_normalize(text: str) -> str:
    new_title = text.lower()
    new_title = "".join(
        c for c in uni.normalize("NFD", new_title) if uni.category(c) != "Mn"
    )
    # delete multi-spaces
    new_title = " ".join(new_title.split())
    chars_to_replace = [" ", "-"]
    for char in chars_to_replace:
        new_title = new_title.replace(char, "_")

    return new_title


def find_or_create_toolbar(iface, title: str) -> QToolBar:
    obj_name = title_normalize(title)
    toolbars = iface.mainWindow().findChildren(QToolBar, obj_name)
    if len(toolbars) >= 1:
        return toolbars[0]
    else:
        new_toolbar = iface.addToolBar(title)
        new_toolbar.setObjectName(obj_name)
        return new_toolbar


class CenQueriesPlugin:
    def __init__(self, iface) -> None:
        self.iface = iface
        self.main_window = None

    def initGui(self) -> None:
        dir_path: str = os.path.dirname(os.path.abspath(__file__))
        self.app_icon: QIcon = QIcon(f"{dir_path}/images/app.svg")
        self.action: QAction = QAction(
            self.app_icon, "Requêtes CEN", self.iface.mainWindow()
        )
        self.action.triggered.connect(self.run)
        self.cen_toolbar = find_or_create_toolbar(self.iface, "CEN Alsace")
        self.cen_toolbar.addAction(self.action)

    def unload(self) -> None:
        self.iface.removeToolBarIcon(self.action)
        del self.main_window
        del self.action

    def run(self) -> None:
        if self.main_window is None:
            self.main_window: CenQueriesWindow = CenQueriesWindow(self.iface)

        self.main_window.show()
